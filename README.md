# Access Score

Accessibility scores for walking, biking, bus, and driving using
[Web Map GL](https://gitlab.com/ccrpc/webmapgl).

## License
Access Score is available under the terms of the
[BSD 3-clause
license](https://gitlab.com/ccrpc/access-score/blob/master/LICENSE.md).
