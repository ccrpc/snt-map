import { h, Component, Element, Event, EventEmitter, Listen, Prop }
  from '@stencil/core';
import { _t } from '../../i18n/i18n';
import { hashManager } from '@ccrpc/hash-manager';


@Component({
  tag: 'as-scenarios'
})
export class Scenarios {
  @Element() el: HTMLElement;

  @Prop({mutable: true}) default: string;
  @Prop({mutable: true}) scenario: HTMLAsScenarioElement;
  @Prop({mutable: true}) compare: boolean = true;

  @Event() asScenarioChange: EventEmitter;

  componentWillLoad() {
    let initial = hashManager.init('scenario', this.default);
    this.scenario = this.getScenario(initial);
    this.compare = hashManager.init('compare', this.compare.toString())
      === 'true';
  }

  @Listen('hashManagerChange', {target: 'window'})
  hashChanged(e: CustomEvent) {
    let newId = e.detail.new.scenario;
    if (this.scenario.id !== newId)
      this.scenario = this.getScenario(newId);
    let newCompare = e.detail.new.compare;
    if (newCompare !== this.compare.toString())
      this.compare = newCompare === 'true';
  }

  getScenario(id: string) : HTMLAsScenarioElement {
    let scenario : HTMLAsScenarioElement =
      this.el.querySelector(`as-scenario#${id}`);
    if (!scenario)
      scenario = this.el.querySelector(`as-scenario#${this.default}`);
    return scenario;
  }

  handleChange(e: CustomEvent) {
    this.scenario = this.getScenario(e.detail.value);
    this.asScenarioChange.emit();
    hashManager.set('scenario', this.scenario.id);
  }

  handleToggle(e: CustomEvent) {
    this.compare = e.detail.checked;
    this.asScenarioChange.emit();
    hashManager.set('compare', this.compare.toString());
  }

  render() {
    return ([
      <ion-item-divider>
        <ion-label>{_t('as.scenarios.label')}</ion-label>
        <as-help-button slot="end" buttonTitle={_t('as.scenarios.info')}
          component="as-scenario-help"></as-help-button>
      </ion-item-divider>,
      <ion-item>
        <ion-icon slot="start" name="options"></ion-icon>
        <ion-select interface="popover"
          value={(this.scenario) ? this.scenario.id : this.default}
          label={_t('as.scenarios.label')}
          labelPlacement="floating"
          onIonChange={(e) => this.handleChange(e)}>
          <slot />
        </ion-select>
      </ion-item>,
      <ion-item>
        <ion-checkbox slot="start" value="compare"
          checked={this.compare}
          disabled={!this.scenario || !this.scenario.difference}
          labelPlacement="end"
          onIonChange={(e) => this.handleToggle(e)}><ion-label>{_t('as.scenarios.compare')}</ion-label></ion-checkbox>
      </ion-item>
    ]);
  }
}
