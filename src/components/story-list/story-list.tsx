import { h, Component, Element } from '@stencil/core';
import { _t } from '../../i18n/i18n';


@Component({
  tag: 'as-story-list'
})
export class StoryList {

  @Element() el: HTMLElement;

  closeModal() {
    let modal = document.querySelector('ion-modal');
    if (modal) modal.dismiss();
  }

  closePopover() {
    this.el.closest('ion-popover').dismiss();
  }

  getStories() {
    return Array.from(document.querySelectorAll('as-story'));
  }

  startStory(current) {
    let drawer = document.querySelector('gl-drawer');
    drawer.open = true;
    this.getStories().forEach((story) => {
      story.active = story === current;
      let glStory = story.querySelector('gl-story');
      glStory.step = (story.active) ? 0 : undefined;
      if (story.active) {
        drawer.drawerTitle = `${_t('as.app.stories')}: ${story.name}`;
        this.el.closest('as-app').storyId = glStory.id;
      }
    });
    
    this.closePopover();
    this.closeModal();
  }

  getItems() {
    let stories = this.getStories();
    return stories.map((story, i) => {
      return (
        <ion-item lines={(i + 1 === stories.length) ? 'none' : 'full'}
            onClick={() => this.startStory(story)} button={true}>
          <ion-label class="ion-text-wrap">{story.name}</ion-label>
        </ion-item>
      );
    });
  }

  render() {
    return ([
      <ion-item-divider>
        <ion-label>{_t('as.app.stories')}</ion-label>
      </ion-item-divider>,
      this.getItems()
    ]);
  }
}

