import { h, Component } from '@stencil/core';
import { _t } from '../../i18n/i18n';
import { getDataUrl } from '../utils';


@Component({
  styleUrl: 'scenario-help.css',
  tag: 'as-scenario-help'
})
export class ScenarioHelp {

  render() {
    return ([
      <as-modal name={_t('as.info-menu.scenario')}>
        <h2>Transportation Vision</h2>
        {/* <div class='embed-container'>
          <iframe src="https://www.youtube-nocookie.com/embed/YeY1VC8UDpg"
            frameborder="0" allowFullScreen></iframe>
        </div> */}
        <p>
          Public input from <a href="https://ccrpc.gitlab.io/lrtp-2050/">Long
            Range Transportation Plan 2050</a> conveyed strong support for a set
          of overlapping ideas about the future of transportation:
        </p>
        <ul>
          <li>A more environmentally-sustainable transportation system</li>
          <li>More bicycle and pedestrian infrastructure</li>
          <li>Shorter off-campus transit times</li>
          <li>Equitable access to transportation services</li>
          <li>A compact urban area that supports active transportation</li>
          <li>Limited sprawl</li>
          <li>Automated vehicles (AVs)</li>
          <li>High speed rail</li>
        </ul>
        <h2>Scenarios</h2>
        <p>Based on this vision for the future, CUUATS LRTP 2050 Steering
          Committee developed three future scenarios, in addition to the 2020
          baseline scenario:
        </p>
        <ul>
          <li>
            <strong>2020 Baseline:</strong> Based on 2020 data,
            it is intended to reflect current conditions.
          </li>
          <li>
            <strong>2050 Business-as-Usual:</strong> Forecasts how and where
            development will occur between 2020 and 2050 based on historic
            development trends, relatively certain future development projects
            and transportation system changes, as well as conservative
            integration of electric and autonomous vehicles.
          </li>
          <li>
            <strong>2050 Optimistic:</strong> Designed to incorporate relatively
            certain future developments and transportation system changes as well
            as Federal, State, and local goals as summarized in the Goals,
            Future Conditions, and Future Projects sections. This scenario reflects
            ambitious implementation of bike and pedestrian recommendations in current plans,
            projected transit system changes, future environmental considerations and actions,
            and an emphasis on infill (over peripheral or sprawl) development.

            It is the preferred scenario for the LRTP 2050.
          </li>
          <li>
            <strong>2050 Transformative:</strong> Incorporates the construction
            of a high speed rail (HSR) line to Chicago by the year 2045 and
            resulting growth in the core area. This scenario reflects more
            access to technology, i.e., more teleworking and a higher adoption
            rate of electric and automated vehicles.
          </li>
        </ul>
        <h2>Population and Employment</h2>
        <p>
          Greater population and employment growth are projected for the
          2050 Transformative Scenario, which includes the High Speed Rail. <a href="https://www.midwesthsr.org/sites/default/files/studies/MHSRA_TranSystems_2012.pdf">
            A network benefits study
          </a> indicated that new households and jobs would be attracted to
          the area.
        </p>
        <rpc-table table-title="Population and Employment Projections"
          url={getDataUrl('scenario-population-employment.csv')}
          textAlignment="l,r"
          download={false}
        ></rpc-table>
        {/* Note: We do not have land use and housing data for the lrtp2050 as we do not have the SCALDS model this year. */}
        {/* <h2>Land Use and Housing</h2>
        <p>
          While Business-as-Usual assumes ongoing development at the
          peripheries of municipal areas, the preferred scenarios implement
          building constraints, limiting new development to the existing
          municipal boundaries. This shift maximizes the utility of existing
          infrastructure and helps to preserve land for agricultural uses.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-land-use.csv')}
          type="horizontalBar"
          xLabel="Percent"
          stacked={true}
          source="CUUATS and Social Cost of Alternative Land Development Scenarios (SCALDS) model"
          chartTitle="Projected Land Use in Champaign County"
          download={false}></rpc-chart>
        <p>
          While Business-as-Usual predicts an increase of 2,021 single family
          homes, the preferred scenarios show much larger increases in
          multi-family housing units. The preferred scenario predicts
          demolition and redevelopment of low-density residential parcels
          to higher density multi-family uses.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-housing.csv')}
          type="horizontalBar"
          xLabel="Housing Units"
          xMin={0}
          source="CUUATS and Social Cost of Alternative Land Development Scenarios (SCALDS) model"
          chartTitle="Projected Total Housing Units in Champaign County"
          download={false}></rpc-chart> */}
        <h2>Travel Patterns</h2>
        <p>
          The total vehicle miles travelled (VMT) is projected to increase by approximately 22%,
          30%, and 38% between 2020 and 2050 under the Business-As-Usual Scenario (BAU),
          Optimistic Scenario, and Transformative Scenario, respectively. These increases
          in VMT are primarily attributed to projected increases in population and employment,
          as well as additional induced trips from C/AVs.

          Furthermore, the expansion of bike and pedestrian infrastructures promotes more trips using those modes in the Optimistic and Transformative Scenarios.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-vmt.csv')}
          type="horizontalBar"
          legend={false}
          xLabel="Vehicle Miles Traveled (Millions)"
          xMin={0}
          source="CUUATS and Champaign County Travel Demand Model (TDM)"
          chartTitle="Projected Vehicle Miles Traveled in the MPA"
          download={false}></rpc-chart>
        <rpc-chart
          url={getDataUrl('scenario-mode-share.csv')}
          type="horizontalBar"
          xLabel="Percent"
          xMin={0}
          xMax={100}
          stacked={true}
          source="CUUATS and Champaign County Travel Demand Model (TDM)"
          chartTitle="Projected Mode Share in the MPA"
          download={false}></rpc-chart>
        <h2>Energy and Emissions</h2>
        {/* <p>
          Increasing density reduces the energy consumed by buildings, as well
          as transportation energy needs. Estimated solar production was
          removed from the predicted consumption. The preferred scenarios
          anticipated a greater adoption of electric vehicles, but the increase
          in solar production offsets this increased demand for electricity.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-energy.csv')}
          type="horizontalBar"
          xLabel="MMBtu"
          xMin={0}
          stacked={true}
          source="CUUATS and Social Cost of Alternative Land Development Scenarios (SCALDS) model"
          chartTitle="Projected Energy Consumption Per Capita in Champaign County"
          download={false}></rpc-chart> */}
        <p>
          The figure below illustrates the modeled annual mobile vehicle emissions in Champaign County.
          The increase in emission rates of certain pollutants (e.g., N2O) from heavy-duty
          vehicles for future years (source: <a href="https://www.epa.gov/system/files/documents/2023-08/moves4-plan-update-nh3-n2o-webinar-2023-07-20.pdf">moves4-plan-update-nh3-n2o-webinar-2023-07-20.pdf</a>),
          along with the increased VMT, leads to a corresponding rise in some pollutants.
          The increased adoption of electric vehicles in future scenarios is projected to notably
          reduce total PM2.5 emissions. However, despite varying rates of electric vehicle penetration
          among the three future scenarios, the differences are not significant.

          This is because the overall increase in total PM2.5 emissions from vehicle activity and the
          additional particulates produced by electric vehicles tire wear (due to their increased weight)
          offset the reductions in exhaust emissions from electric vehicle adoption.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-emissions.csv')}
          type="horizontalBar"
          aspectRatio="1.25"
          xLabel="U.S. Ton"
          xMin={0}
          source="CUUATS and the MOtor Vehicle Emission Simulator (MOVES) model"
          chartTitle="Projected Emissions in the MPA"
          download={false}></rpc-chart>
        {/* <h2>Health</h2>
        <p>
          People who routinely walk and bike are more likely to get the
          recommended levels of daily physical activity for good health,
          which can lower the risk of certain health conditions such as
          obesity, hypertension, and type II diabetes. Preliminary
          modeling suggests that bicycle and pedestrian
          accessibility improvements in the 2045 Preferred scenario could
          lead to 292 fewer patients (a 1.7% reduction) requiring treatment for
          these diseases per year compared to the 2015 Baseline scenario.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-health.csv')}
          type="horizontalBar"
          xLabel="Patients Treated per Year"
          xMin={0}
          legend={false}
          aspectRatio={3}
          source="CUUATS Health Impact Assessment"
          chartTitle="Patients Treated for Selected Conditions in the UA"
          download={false}></rpc-chart> */}
        <h2>More Information</h2>
        <p>
          For more information please visit the LRTP 2050 <a href="https://ccrpc.gitlab.io/lrtp-2050/vision/model/">Scenario Modelling</a> page.
        </p>
      </as-modal>
    ]);
  }
}
