import { h, Component, Element, Prop } from '@stencil/core';
import { _t } from '../../i18n/i18n';


@Component({
  tag: 'as-toggles'
})
export class Toggles {
  @Element() el : HTMLElement;

  @Prop() helpComponent: string;
  @Prop() helpTitle: string;
  @Prop() type: 'modes' | 'destinations';

  getHelpButton() {
    if (!this.helpComponent) return;

    return (
      <as-help-button slot="end"
        component={this.helpComponent}
        buttonTitle={_t(this.helpTitle)}></as-help-button>
    );
  }

  toggleAll() {
    let toggles = Array.from(this.el.querySelectorAll('as-toggle'));
    let allEnabled = toggles.map((t) => t.enabled).reduce((a, b) => a && b);
    toggles.forEach((t) => t.enabled = !allEnabled);
  }

  render() {
    return (
      <ion-item-group>
        <ion-item-divider>
          <ion-label>{_t(`as.app.groups.${this.type}`)}</ion-label>
          <ion-buttons slot="end">
            {this.getHelpButton()}
            <ion-button fill="clear" color="medium"
                title={_t('as.toggles.all')} onClick={() => this.toggleAll()}>
              <ion-icon name="switch" slot="icon-only"></ion-icon>
            </ion-button>
          </ion-buttons>
        </ion-item-divider>
        <slot />
      </ion-item-group>
    );
  }
}
