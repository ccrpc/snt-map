#!/bin/sh

# Data sources: PostGIS and GeoPackage
GPKG=/data/transformative-2050.gpkg
MBTILES=/data/transformative-2050.mbtiles

# Include helpers
source /tiles/tile_helpers.sh

read -r -d '' segment_query <<EOM
SELECT *
FROM access_score.access_score_2050_transformative
EOM

read -r -d '' destination_query <<EOM
SELECT *
FROM access_score.destinations_2050
EOM

read -r -d '' project_query <<EOM
SELECT COALESCE(name, streetname) AS name, geom
FROM access_score.projects_2050_vision
WHERE lrtp_2045 = 'Proposed'
UNION
SELECT name, geom
FROM access_score.projects_2050_champaign_gtk
EOM

remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "segment" "0" "14" "$segment_query"
add_layer "destination" "12" "14" "$destination_query"
add_layer "project" "0" "14" "$project_query"

# Convert GeoPackage to MBTiles
export_tiles \
    "Transformative Scenario for 2050" \
    "Accessibility scores from the Sustainable Neighborhoods analysis of Champaign County, Illinois" \
    "0" "14"
