#!/bin/sh

# Data sources: PostGIS and GeoPackage
GPKG=/data/optimistic-2050-difference.gpkg
MBTILES=/data/optimistic-2050-difference.mbtiles

# Include helpers
source /tiles/tile_helpers.sh

read -r -d '' segment_query <<EOM
SELECT
    access_score_2050_optimistic.segment_id as segment_id,
    access_score_2050_optimistic.geom as geom,
    access_score_2050_optimistic.name as name,
    access_score_2050_optimistic.cross_name_start as cross_name_start,
    access_score_2050_optimistic.cross_name_end as cross_name_end,
    access_score_2050_optimistic.bus_grocery           - access_score_baseline_2020.bus_grocery AS bus_grocery,
    access_score_2050_optimistic.bus_art               - access_score_baseline_2020.bus_art AS bus_art,
    access_score_2050_optimistic.bus_restaurant        - access_score_baseline_2020.bus_restaurant AS bus_restaurant,
    access_score_2050_optimistic.bus_job               - access_score_baseline_2020.bus_job AS bus_job,
    access_score_2050_optimistic.bus_retail            - access_score_baseline_2020.bus_retail AS bus_retail,
    access_score_2050_optimistic.bus_service           - access_score_baseline_2020.bus_service AS bus_service,
    access_score_2050_optimistic.bus_park              - access_score_baseline_2020.bus_park AS bus_park,
    access_score_2050_optimistic.pedestrian_grocery    - access_score_baseline_2020.pedestrian_grocery AS pedestrian_grocery,
    access_score_2050_optimistic.pedestrian_art        - access_score_baseline_2020.pedestrian_art AS pedestrian_art,
    access_score_2050_optimistic.pedestrian_restaurant - access_score_baseline_2020.pedestrian_restaurant AS pedestrian_restaurant,
    access_score_2050_optimistic.pedestrian_job        - access_score_baseline_2020.pedestrian_job AS pedestrian_job,
    access_score_2050_optimistic.pedestrian_retail     - access_score_baseline_2020.pedestrian_retail AS pedestrian_retail,
    access_score_2050_optimistic.pedestrian_service    - access_score_baseline_2020.pedestrian_service AS pedestrian_service,
    access_score_2050_optimistic.pedestrian_park       - access_score_baseline_2020.pedestrian_park AS pedestrian_park,
    access_score_2050_optimistic.bicycle_grocery       - access_score_baseline_2020.bicycle_grocery AS bicycle_grocery,
    access_score_2050_optimistic.bicycle_art           - access_score_baseline_2020.bicycle_art AS bicycle_art,
    access_score_2050_optimistic.bicycle_restaurant    - access_score_baseline_2020.bicycle_restaurant AS bicycle_restaurant,
    access_score_2050_optimistic.bicycle_job           - access_score_baseline_2020.bicycle_job AS bicycle_job,
    access_score_2050_optimistic.bicycle_retail        - access_score_baseline_2020.bicycle_retail AS bicycle_retail,
    access_score_2050_optimistic.bicycle_service       - access_score_baseline_2020.bicycle_service AS bicycle_service,
    access_score_2050_optimistic.bicycle_park          - access_score_baseline_2020.bicycle_park AS bicycle_park,
    access_score_2050_optimistic.vehicle_grocery       - access_score_baseline_2020.vehicle_grocery AS vehicle_grocery,
    access_score_2050_optimistic.vehicle_art           - access_score_baseline_2020.vehicle_art AS vehicle_art,
    access_score_2050_optimistic.vehicle_restaurant    - access_score_baseline_2020.vehicle_restaurant AS vehicle_restaurant,
    access_score_2050_optimistic.vehicle_job           - access_score_baseline_2020.vehicle_job AS vehicle_job,
    access_score_2050_optimistic.vehicle_retail        - access_score_baseline_2020.vehicle_retail AS vehicle_retail,
    access_score_2050_optimistic.vehicle_service       - access_score_baseline_2020.vehicle_service AS vehicle_service,
    access_score_2050_optimistic.vehicle_park          - access_score_baseline_2020.vehicle_park AS vehicle_park,
    access_score_2050_optimistic.bicycle_health        - access_score_baseline_2020.bicycle_health AS bicycle_health,
    access_score_2050_optimistic.pedestrian_health     - access_score_baseline_2020.pedestrian_health AS pedestrian_health,
    access_score_2050_optimistic.bus_health            - access_score_baseline_2020.bus_health AS bus_health,
    access_score_2050_optimistic.vehicle_health        - access_score_baseline_2020.vehicle_health AS vehicle_health,
    access_score_2050_optimistic.bicycle_public        - access_score_baseline_2020.bicycle_public AS bicycle_public,
    access_score_2050_optimistic.pedestrian_public     - access_score_baseline_2020.pedestrian_public AS pedestrian_public,
    access_score_2050_optimistic.bus_public            - access_score_baseline_2020.bus_public AS bus_public,
    access_score_2050_optimistic.vehicle_public        - access_score_baseline_2020.vehicle_public AS vehicle_public,
    access_score_2050_optimistic.bicycle_school        - access_score_baseline_2020.bicycle_school AS bicycle_school,
    access_score_2050_optimistic.pedestrian_school     - access_score_baseline_2020.pedestrian_school AS pedestrian_school,
    access_score_2050_optimistic.bus_school            - access_score_baseline_2020.bus_school AS bus_school,
    access_score_2050_optimistic.vehicle_school        - access_score_baseline_2020.vehicle_school AS vehicle_school
FROM access_score.access_score_2050_optimistic
JOIN access_score.access_score_baseline_2020
  ON access_score_2050_optimistic.segment_id = access_score_baseline_2020.segment_id
EOM

read -r -d '' destination_query <<EOM
SELECT *
FROM access_score.destinations_2050
EOM

read -r -d '' project_query <<EOM
SELECT COALESCE(name, streetname) AS name, geom
FROM access_score.projects_2050_vision
WHERE lrtp_2045 = 'Proposed'
UNION
SELECT name, geom
FROM access_score.projects_2050_champaign_gtk
EOM

remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "segment" "0" "14" "$segment_query"
add_layer "destination" "12" "14" "$destination_query"
add_layer "project" "0" "14" "$project_query"

# Convert GeoPackage to MBTiles
export_tiles \
    "Difference Between the Optimisic Scenario Accessibility prediction for 2050 and 2020 Baseline" \
    "Accessibility scores from the Sustainable Neighborhoods analysis of Champaign County, Illinois" \
    "0" "14"
