#!/bin/bash

docker build -t access-score.tiles:export .
docker run -v $PWD/data:/data --env-file .env --name export_tiles access-score.tiles:export
docker container rm export_tiles
docker image rm access-score.tiles:export
